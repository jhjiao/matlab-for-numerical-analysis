% Simulation assignment 4
% Created on: Nov 27, 2020
% Thanks to help and support from Prof. Ayman El-hag & TA Aram Kirakosyan

% import data from the file, make sure that the txt file are found within the same directory as this code file
data = importdata ("test_1.txt");
x = data(:,1);
y = data (:,2);

%ask for input
disp ("What would you like to do?");
model_choose = input ("(Enter digit only) 1-Derivative 2-Integration:   ");

if model_choose == 1
    %do derivative
        disp ("You have selected: Derivative.");
        point_choose = input ("Enter point: ");
        %check for if point is outside of range to do CDD
        if (point_choose <= min (x)) || (point_choose >= max (x))
            disp ("ERROR: POINT IS OUT OF RANGE TO PERFORM CDD METHOD!!!");
        else
            %point is good -> proceed to do derivative
            [slope] = derivative (x,y,point_choose);
            %display the results
            disp ("Derivative is: ");
            disp (slope);
        end
else
    %do integration
    disp ("You have selected: Integration.");
    p1 = input ("Enter the lower limit of integration: ");
    p2 = input ("Enter the upper limit of integration: ");
    n = input ("Enter the # of segments desired: ");
    %check for validity of input
    if (p1 < min(x)) || (p2 > max (x)) 
        disp ("ERROR: LIMITS OF INTEGRATION MUST BE WITHIN RANGE OF DATASET PROVIDED!!!");
    elseif (p1 >= p2)
        disp ("ERROR: LOWER LIMIT MUST BE LESS THAN UPPER LIMIT!!!")
    else
        %make function call to derivative function
        [result] = integral (x,y,p1,p2,n);
        %display results
        disp ("Integration result is: ");
        disp (result);
    end
end

function [slope] = derivative (x,y,point)
%find function to check if the point exists within the set x
p = find (x==point);
% ~isempty(p) will tell whether the point is contained within x the latter half conditions check for even spacing; used abs() to find absolute value
if (~isempty (p))&& (abs(x(p(1)) - x (p(1)-1)) == abs (x(p(1)) - x (p(1)+1)))
    %condition satisfied? --> calculate slope directly and return from function
    slope = (y(p(1)+1) - y(p(1)-1))/(2*(x(p(1)) - x (p(1)-1)));
    return;
end
%use cubic regression to find model (cubic minimizes error; R-squared
%closest to 1 compared to other models from previous simulation assignment)
[a0,a1,a2,a3] = cubic_regression_for_derivative (x,y, point);
%find the minimum of x increment
delta_h = abs(min (diff(x)));
%find y1 and y2; evaluate() is a function that computes function value at a given point 
[y1] = evaluate (a0,a1,a2,a3,point-delta_h);
[y2] = evaluate (a0,a1,a2,a3, point + delta_h);
% apply CDD method formula
slope = (y2 - y1) / (2 * delta_h);
end

function [area] = integral (x,y, p1, p2, n)
%find increment delta x
inc = ( p2 - p1 )/n;
%generate new data set new_x with p1, p2 and the calculated increment size
new_x = [p1:inc:p2];
%new_x is a row vector, change to column vector to match that of x
new_x = transpose (new_x);
% check if p1 and p2 are contained within x
index_p1 = find (x == p1);
index_p2 = find (x == p2);
% use summation to accumulate the summation result found in trapezoidal
% rule formula
summation = 0;
%index_p1 and index_p2 not empty? ->p1 and p2 within x data set
if ~isempty (index_p1) && ~isempty(index_p2)
    %extract subarray from x of the range p1 to p2, use index_p1 because
    %this usage requires indices rather than direct values
    old_x = x (index_p1(1) : index_p2(1));
    % check if old_x is equal to new_x
    if isequal (old_x, new_x)
        % get the indices ready for later accesses of y values
    ind_low = index_p1 (1);
    ind_high = index_p2 (1);
    for i = 1:(n-1)
        %find summation through each iteration of the loop
        summation  = summation + y (ind_low + i *  inc);
    end
    %calculate area and return directly from function - skip the rest of
    %the function
    area = (p2 - p1) / 2 / n * ( y (ind_low) + 2 * summation + y (ind_high));
    return;
    end
end
%p1 and p2 not from original data set AND/OR new data set (new_x) contains
%new elements
[a0,a1,a2,a3] = cubic_regression_for_integration (x,y);
for p = 1:(n-1)
    %calculate summation; use evaluate () to compute the function value
    [y_int] = evaluate (a0, a1, a2, a3, p1 + p * inc);
    summation = summation + y_int;
end
%evaluate at the end points p1 and p2
[y_p1] = evaluate (a0, a1, a2, a3, p1);
[y_p2] = evaluate (a0, a1, a2, a3, p2);
%apply trapezoidal rule and end of function
area = (p2 - p1) / 2 / n * ( y_p1 + 2 * summation + y_p2);
end

%cubic regression model (copied from simulation assignment 3)
function [a0, a1, a2, a3] = cubic_regression_for_derivative (x, y, x_desired)
n = size (x);
%find the corresponding matrix using lecture notes formulae
matrix = zeros (4,4);
matrix (1,1) = n(1,1);
matrix (1,2) = sum(x);
matrix (1,3) = sum (x.^2);
matrix (1,4) = sum (x.^3);
matrix (2,1) = sum (x);
matrix (2,2) = sum (x.^2);
matrix (2,3) = sum (x.^3);
matrix (2,4) = sum (x.^4);
matrix (3,1) = sum (x.^2);
matrix (3,2) = sum (x.^3);
matrix (3,3) = sum (x.^4);
matrix (3,4) = sum (x.^5);
matrix (4,1) = sum (x.^3);
matrix (4,2) = sum (x.^4);
matrix (4,3) = sum (x.^5);
matrix (4,4) = sum (x.^6);

%find column vector b
b = zeros (4,1);
b (1,1) = sum (y);
b (2,1) = sum (x.*y);
b (3,1) = sum ((x.^2).*y);
b (4,1) = sum ((x.^3).*y);

% solve for coefficients and extract to a0, a1, a2, a3
coeffs = matrix \ b;
a0 = coeffs (1,1);
a1 = coeffs (2,1);
a2 = coeffs (3,1);
a3 = coeffs (4,1);
%find R-squared
St =  (y  - mean (y)) .^2;
Sr = (y  - a1 .* x - a0 - a2.*(x.^2) - a3 .* (x.^3)) .^2;
r_sqrd = (sum(St) - sum(Sr))/sum(St);

%plot the graph
close all;
scatter(x,y) ;
xlabel('x');
ylabel('y');
hold on;
% xlim [(x_desired - 5) (x_desired + 5)]
plot (x, a0 + a1 .*x + a2 .* x.^2 + a3 .* x.^3);
plot (x_desired, a0 + a1 *x_desired + a2 * x_desired^2 + a3 * x_desired^3, 'r*');
hold on;
gravstr='Polynomial, y = ';
for i=0:3
    if i==0
        gravstr=[gravstr sprintf('%0.4f x^{3} + ',[a3])];
    elseif i == 1
        gravstr=[gravstr sprintf('%0.4f x^{2} + ',[a2])];
    elseif i == 2
         gravstr=[gravstr sprintf('%0.4f x^{1} + ',[a1])];
    else
        gravstr=[gravstr sprintf('%0.4f',[a0])];
    end
end
gravstr=[gravstr sprintf('; R^2 = %0.4f',[r_sqrd])];
legend ('actual data', gravstr);
end

%%evaluate function - computes a cubic polynomial at point x
function [y_point] = evaluate (a0,a1,a2,a3,x)
y_point = a0 + a1 * x + a2 * x * x + a3 * x * x * x;
end

%cubic regression model for integration
function [a0, a1, a2, a3] = cubic_regression_for_integration (x, y)
n = size (x);
%find the corresponding matrix using lecture notes formulae
matrix = zeros (4,4);
matrix (1,1) = n(1,1);
matrix (1,2) = sum(x);
matrix (1,3) = sum (x.^2);
matrix (1,4) = sum (x.^3);
matrix (2,1) = sum (x);
matrix (2,2) = sum (x.^2);
matrix (2,3) = sum (x.^3);
matrix (2,4) = sum (x.^4);
matrix (3,1) = sum (x.^2);
matrix (3,2) = sum (x.^3);
matrix (3,3) = sum (x.^4);
matrix (3,4) = sum (x.^5);
matrix (4,1) = sum (x.^3);
matrix (4,2) = sum (x.^4);
matrix (4,3) = sum (x.^5);
matrix (4,4) = sum (x.^6);

%find column vector b
b = zeros (4,1);
b (1,1) = sum (y);
b (2,1) = sum (x.*y);
b (3,1) = sum ((x.^2).*y);
b (4,1) = sum ((x.^3).*y);

% solve for coefficients and extract to a0, a1, a2, a3
coeffs = matrix \ b;
a0 = coeffs (1,1);
a1 = coeffs (2,1);
a2 = coeffs (3,1);
a3 = coeffs (4,1);
%find R-squared
St =  (y  - mean (y)) .^2;
Sr = (y  - a1 .* x - a0 - a2.*(x.^2) - a3 .* (x.^3)) .^2;
r_sqrd = (sum(St) - sum(Sr))/sum(St);

%plot the graph
close all;
scatter(x,y) ;
xlabel('x');
ylabel('y');
hold on;
plot (x, a0 + a1 .*x + a2 .* x.^2 + a3 .* x.^3);

gravstr='Polynomial, y = ';
for i=0:3
    if i==0
        gravstr=[gravstr sprintf('%0.4f x^{3} + ',[a3])];
    elseif i == 1
        gravstr=[gravstr sprintf('%0.4f x^{2} + ',[a2])];
    elseif i == 2
         gravstr=[gravstr sprintf('%0.4f x^{1} + ',[a1])];
    else
        gravstr=[gravstr sprintf('%0.4f',[a0])];
    end
end
gravstr=[gravstr sprintf('; R^2 = %0.4f',[r_sqrd])];
legend ('actual data', gravstr);
end