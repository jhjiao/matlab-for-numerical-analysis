% Note:  
%PLEASE RUN MY CODE WITH MATLAB VERSION R2019a or later
%THIS IS BECAUSE I HAVE USED MATLAB BUILT-IN FUNCTION RMMISSING(), WHICH IS
%ONLY AVAILABLE AFTER MATLAB R2019a. THANK YOU!

%% import data from the file, make sure that the txt file are found within the same directory as this
%code file
data = importdata ("test3.txt");
% data = importdata ("test2.txt");
x = data(:,1);
y = data (:,2);

%% display the instruction to the user for input
disp ("Choose the following models: 1 - Polynomial;  2 - Exponential; 3 - Saturation");
%%  make the user choose model
model_choose = input ("Enter your desired model from 1 of the 3 options listed above (enter number only): ");

%% model_choose is 
%1 means polynomial is selected
%2 means exponential is selected
%3 means saturation is selected
if model_choose == 1
    %% ask for the degree of polynomial model
    degree = input ("Enter degree of the polynomial (1,2,3): ");
    if degree == 1
    %% degree is 1 means linear regression - call linear_regres model
        %with x and y arrays
        [a0, a1, r_sqrd] = linear_regres (x, y);
        %a0 is constant term, a1 is the x term, r_sqrd is R squared value
        close all;
        %plot the points
        scatter(x,y) ;
        xlabel('x');
        ylabel('y');
        hold on;
        plot (x, a0 + a1 .*x);
        gravstr='Polynomial, y = ';
        for i=0:1
            if i==0
                gravstr=[gravstr sprintf('%0.4f x^{1} + ',[a1])];
            else
                gravstr=[gravstr sprintf('%0.4f',[a0])];
            end
        end
        gravstr=[gravstr sprintf('; R^2 = %0.4f',[r_sqrd])];
        legend ('actual data', gravstr);
     elseif degree == 2
         %% user selected quadratic polynomial - function call to
         %quadratic_regres
         [a0, a1, a2, r_sqrd] = quadratic_regres (x, y);
        close all;

        scatter(x,y) ;
        xlabel('x');
        ylabel('y');
        hold on;
        plot (x, a0 + a1 .*x + a2 .* x.^2);
        gravstr='Polynomial, y = ';
        for i=0:2
            if i==0
                gravstr=[gravstr sprintf('%0.4f x^{2} + ',[a2])];
            elseif i == 1
                 gravstr=[gravstr sprintf('%0.4f x^{1} + ',[a1])];
            else
                gravstr=[gravstr sprintf('%0.4f',[a0])];
            end
        end
        gravstr=[gravstr sprintf('; R^2 = %0.4f',[r_sqrd])];
        legend ('actual data', gravstr);
    else
         %% else means degree is 3, which is cubic - function call to
         % cubic_regres
         [a0, a1, a2, a3, r_sqrd] = cubic_regres (x, y);
         close all;
        scatter(x,y) ;
        xlabel('x');
        ylabel('y');
        hold on;
        plot (x, a0 + a1 .*x + a2 .* x.^2 + a3 .* x.^3);
        gravstr='Polynomial, y = ';
        for i=0:3
            if i==0
                gravstr=[gravstr sprintf('%0.4f x^{3} + ',[a3])];
            elseif i == 1
                gravstr=[gravstr sprintf('%0.4f x^{2} + ',[a2])];
            elseif i == 2
                 gravstr=[gravstr sprintf('%0.4f x^{1} + ',[a1])];
            else
                gravstr=[gravstr sprintf('%0.4f',[a0])];
            end
        end
        gravstr=[gravstr sprintf('; R^2 = %0.4f',[r_sqrd])];
        legend ('actual data', gravstr);
    end
 elseif model_choose == 2
     %% user selected 2 - exponential model to be computed - function call to
     %expo()
        [a, b, r_sqrd, X, Y]= expo (x,y);
        close all;
        scatter(x,y) ;
        xlabel('x');
        ylabel('y');
        hold on;
        plot (x, a * exp (b.*x));
        gravstr='Exponential, y = ';
        gravstr=[gravstr sprintf('%0.4f e^{%0.4f x}',[a,b])];
        gravstr=[gravstr sprintf('; R^2 = %0.4f',[r_sqrd])];
        legend ('actual data',gravstr);
else
        %% user entered 3 - model_choose is 3 - saturation model - function call
        %to saturation (x,y)
        [a, b, r_sqrd,X,Y] = saturation (x,y);
        close all;
        scatter(x,y) ;
        xlabel('x');
        ylabel('y');
        hold on;
        plot (x, (a .*x) ./ (b + x));
        gravstr='Saturation, y = ';
        for i=0:1
            if i==0
                gravstr=[gravstr sprintf('%0.4f x^{1} ',[a])];
            else
                gravstr=[gravstr sprintf('/ (%0.4f + x^{1})',[b])];
            end
        end
        gravstr=[gravstr sprintf('; R^2 = %0.4f',[r_sqrd])];
        legend ('actual data', gravstr);
 end

%% linear regression 
function [a0, a1, r_sqrd] = linear_regres (x, y)
% find size of input
n = size (x);
%initialize variables to be used as sums
sum_xy = 0;
sum_x = 0;
sum_y = 0;
sum_x_sqrd = 0;

%compute each term in the linear regressio model separately
for i = 1:n
    sum_xy = sum_xy + x(i) * y(i);
    sum_x = sum_x + x(i);
    sum_y = sum_y + y (i);
    sum_x_sqrd = sum_x_sqrd + x(i)^2;
end

%find a1, a0 using the formula from lecture notes
a1 = (n (1,1)*sum_xy - sum_x * sum_y) / (n(1,1)*sum_x_sqrd - sum_x ^2);
a0 = mean (y) - a1 * mean (x);

%find R^2 value from St and Sr
St =  (y  - mean (y)) .^2;
Sr = (y  - a1 .* x - a0 ) .^2;
r_sqrd = (sum(St) - sum(Sr))/sum(St);
%end of function
end

%% Quadratic polynomial regression model
function [a0, a1, a2, r_sqrd] = quadratic_regres (x, y)
n = size (x);
%initialize matrix with zeros to begin with
matrix = zeros (3,3);
%fill matrix with values as laid out in lecture notes on polynomial
%regression
matrix (1,1) = n(1,1);
matrix (1,2) = sum(x);
matrix (1,3) = sum (x.^2);
matrix (2,1) = sum (x);
matrix (2,2) = sum (x.^2);
matrix (2,3) = sum (x.^3);
matrix (3,1) = sum (x.^2);
matrix (3,2) = sum (x.^3);
matrix (3,3) = sum (x.^4);
%fill out the column vector b in order to have matrix x = b
b = zeros (3,1);
b (1,1) = sum (y);
b (2,1) = sum (x.*y);
b (3,1) = sum ((x.^2).*y);

%solve for "x" which is the coefficients vector
coeffs = matrix \ b;
%extract a0, a1, a2
a0 = coeffs (1,1);
a1 = coeffs (2,1);
a2 = coeffs (3,1);

%find R squared
St =  (y  - mean (y)) .^2;
Sr = (y  - a1 .* x - a0 - a2.*(x.^2)) .^2;
r_sqrd = (sum(St) - sum(Sr))/sum(St);

end

%% Cubic polynomial regression model
function [a0, a1, a2, a3, r_sqrd] = cubic_regres (x, y)
n = size (x);
%find the corresponding matrix using lecture notes formulae
matrix = zeros (4,4);
matrix (1,1) = n(1,1);
matrix (1,2) = sum(x);
matrix (1,3) = sum (x.^2);
matrix (1,4) = sum (x.^3);
matrix (2,1) = sum (x);
matrix (2,2) = sum (x.^2);
matrix (2,3) = sum (x.^3);
matrix (2,4) = sum (x.^4);
matrix (3,1) = sum (x.^2);
matrix (3,2) = sum (x.^3);
matrix (3,3) = sum (x.^4);
matrix (3,4) = sum (x.^5);
matrix (4,1) = sum (x.^3);
matrix (4,2) = sum (x.^4);
matrix (4,3) = sum (x.^5);
matrix (4,4) = sum (x.^6);

%find column vector b
b = zeros (4,1);
b (1,1) = sum (y);
b (2,1) = sum (x.*y);
b (3,1) = sum ((x.^2).*y);
b (4,1) = sum ((x.^3).*y);

% solve for coefficients and extract to a0, a1, a2, a3
coeffs = matrix \ b;
a0 = coeffs (1,1);
a1 = coeffs (2,1);
a2 = coeffs (3,1);
a3 = coeffs (4,1);

%find R squared
St =  (y  - mean (y)) .^2;
Sr = (y  - a1 .* x - a0 - a2.*(x.^2) - a3 .* (x.^3)) .^2;
r_sqrd = (sum(St) - sum(Sr))/sum(St);
end

%% Exponential model
function [a,b,r_sqrd,X,Y] = expo (x,y)
   n = size(x);
   %n is a row vector, actual size is in (1,1)
   %take natural log for each element of y and store them in y_ln
   y_ln = zeros (n(1,1),1);
   for i = 1:n(1,1)
       %if the element >0  no problem
       if y(i,1)>0
       y_ln (i,1) = log(y(i,1));
       else
        %element <= 0, natural log is undefined - throw invalide logarithm
        %warning
       	disp ("Invalid logarithm");
        % set that entry to NaN and will take care of it in a few lines
        y_ln (i,1) = NaN;
       end
   end
   
for p = 1:n
    % iterate through the vector, for every y element that is NaN, set the
    % corresponding x entry to NaN
    if isnan(y_ln (p,1))
        x (p,1) = NaN;
    end
end

%remove all of the NaN entries in y_ln and x y using rmmissing () function
%(Matlab R2018b or later versions) and store them into X and Y arrays
Y = rmmissing (y_ln);
X = rmmissing (x);
%call linear regression function with the new X and Y
[a0, a1, r] = linear_regres (X, Y);

%find a and b from a0 and a1
b = a1;
a = exp (a0);

%find R squared value
St =  (y  - mean (y)) .^2;
Sr = (y  - a*exp(b.*x)) .^2;
r_sqrd = (sum(St) - sum(Sr))/sum(St);
%end of function
end

%% Saturation model
function [a,b,r_sqrd,X,Y] = saturation (x,y)
%find the size
   n = size(x);
   %initialize 2 arrays to store x and y inverses (1/x and 1/y)
   y_inv = zeros (n(1,1),1);
   x_inv = zeros (n(1,1),1);
   
   %iterate through x and y entry-by-entry and find inverse; if the entry
   %is 0, then throw a division by zero warning and set the entry in *_inv
   %to NaN 
   for i = 1:n
       if y(i,1) ~= 0
       y_inv (i,1)= 1/y(i,1);
       else
       	disp ("Division by zero");
        y_inv (i,1) = NaN;
       end
       if x(i,1) ~= 0
                  x_inv (i, 1) = 1/x(i,1);
        else
       	disp ("Division by zero");
        x_inv (i,1) = NaN;
       end
   end

   %for every y_inv that is nan, set the corresponding x_inv entry to NaN
   %and vice versa
for p = 1:n
    if isnan(y_inv (p,1))
        x_inv (p,1) = NaN;
    end
    if isnan(x_inv(p,1))
        y_inv (p,1) = NaN;
    end
end

%use rmmissing () to remove all NaN entries
X = rmmissing (x_inv);
Y = rmmissing (y_inv);

%call linear_regres to find a0 and a1
[a0, a1, r] = linear_regres (X,Y);

%find a and b
a = 1/a0;
b = a1 * a;

%find R^2 and end the function
St =  (y  - mean (y)) .^2;
Sr = (y  - ((a.*x) ./ (b+x))) .^2;

r_sqrd = (sum(St) - sum(Sr))/sum(St);
% disp ("--------- output ----------");
% disp (r_sqrd);
%end of function
end

% end of simulation assignment 3 code :)