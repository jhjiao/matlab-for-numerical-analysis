A = readmatrix ('A.txt');
B = readmatrix ('B.txt');
format longg;

x = inv_sol (A,B);
disp(x);
y = gauss_elim (A, B);
disp(y);
A_new = 1.05*A;
y_new = gauss_elim (A_new, B);
disp (y_new);
diffe = abs (y_new - y)/y_new;
diffe = round (diffe, 5, "significant");
disp (diffe);
% 
% z = gauss_seidel (A,B, 0.01);
% disp(z);
% a = gauss_seidel (A,B, 0.005);
% disp(a);
% b = gauss_seidel (A,B, 0.001);
% disp(b);
c = gauss_seidel (A,B, 0.0001);
disp(c);

%% inverse solver just solves the linear system of equations with the inverse
%of matrix A multiplied by matrix B
function [x] = inv_sol (A,B)
x = A\ B;
x = round(x, 5,'significant');
disp ("matrix inverse: ");
end

%% Gauss Elimination w/ partial pivoting
function [y] = gauss_elim (A,b) 
Ab = [A, b];
%find the number of rows
no_rows = size (A,1);

for  i = 1:(no_rows)
   %partial pivoting starts
   alpha = abs(Ab);
    %alpha is the column vector that will be used to compare with the pivot
    %element of the matrix
   alpha = alpha (i+1:end,i);
   if (max(alpha) > abs (Ab(i,i)))
        max_index = find(alpha == max(alpha));
        disp(max_index);
        %add 1 to the maximum index because the alpha vector has 1 element
        %less than the column where the pivot is situated at - this was
        %determined during set up of alpha in line 33
        
        %swap the 2 rows
        temp = Ab(max_index+1, :);
        Ab(max_index+1,:) = Ab(i,:);
        Ab(i, :) = temp;
    end
    %partial pivoting completed
    %row-reduction starts
      factor = Ab (i+1:end,i) / Ab(i,i);
      Ab (i+1:end, :) = Ab (i+1:end,:) - factor .* Ab(i,:);
      %row-reduction ends
end
y = zeros (no_rows,1);
%find the last root using the last row
y(end) = Ab (end, end) / Ab (end, end-1);
%below is for backward substitution
for j = (no_rows-1):-1:1
    %y (j) =round( ( Ab (j, end) -  Ab (j,j+1:no_rows) * y (j+1:no_rows))/ Ab (j, j),5,'significant');
y (j) = (Ab (j, end) -  Ab (j,j+1:no_rows) * y (j+1:no_rows))/ Ab (j, j);
end
y = round(y, 5, 'significant');
disp ("below is for gauss elimination result:");
end

%% Gauss-Seidel Method
function [z] = gauss_seidel (A,b, allowed_error)
%find the augmented matrix
Ab = [A,b];
no_rows = size(Ab,1);
%initialize the solution to the matrix as 0s (also my initial guess)
z = zeros (no_rows, 1);
approx_err = zeros (no_rows,1);
num_iterations = 0;
while true
    for k = 1:no_rows
        %use the formula given in lecture notes for Gauss-Seidel
        z_old = z(k);
        numerator = Ab(k,end) - Ab (k,1:k-1)*z(1:k-1)-Ab(k,k+1:no_rows) * z (k+1:no_rows);
        denominator = Ab(k,k);
        z (k) = round(numerator / denominator, 5, 'significant');
        %find the absolute relative approximate error: absolute value of
        %present z - old z ; divided by the current approximation of z
        approx_err (k) = abs((z(k) - z_old) /z(k));
    end
    num_iterations = num_iterations + 1;
    %if the maximum error in the error vector is less than the allowed
    %error, then the loop stops and no further iteration is required.
    if max(approx_err) < allowed_error
        break;
    end
end
disp ('Gauss-Seidel: allowed error (unit: %) and number of iterations are respectively:');
disp (allowed_error*100);
disp (num_iterations);
disp ('The solution after using Gauss-Seidel is \n');
end