syms x y z

f1 = 3+2*1/x/x - 1/8*(3-2*y)/(1-y)*z*z*x*x-4.5;
f2 = 6*y-1/2*y/(1-y)*z*z*x*x-2.5;
f3 = 3-2/x/x-1/8*(1+2*y)/(1-y)*z*z*x*x-0.5;

var = [x;y;z];
%jaco_f is the jacobian matrix
jaco_f = jacobian ([f1,f2,f3], var);
%here, x_prev is the initial guess
x_prev= [1.9,1.6,1];
x_curr = [1,1,1];
%define the error as 100 % as a starting point
err = 100;
error_vec = [0,0,0];
% count is the # of iterations
count = 0;

%loop for newton_raphson; stop criterion is the error
while err>0.1
%evaluate the jacobian at the current guess
jaco_f_guess = subs(jaco_f, [x,y,z],x_curr);
%find the function value at the current guess
f_val = subs ([f1,f2,f3], [x,y,z], x_curr);
%calculate the error in x; functionc all to gauss_elim (copied from sim
%asg.1)
x_offset = gauss_elim (jaco_f_guess, -f_val); 
%update x_curr with the offset calculated
x_curr = x_prev + x_offset; 
% disp ("---------new iteration ----------");
% disp (x_curr);
%update number of iterations
count = count + 1;

%error_vec is the absolute relative error (a column vector which contains
%errors for x, y, and z respectively
error_vec = abs ((x_curr - x_prev)./x_curr) .*100;

%stop condition is that each x, y, z's error must be less than 0.1, so if
%the maximum of the vector is less than 0.1, we are done
err = max(error_vec);
%update x_prev (the previous guess to the current guess
x_prev = x_curr;
end

disp ("------- NEWTON-RAPHSON RESULTS -------");
disp("Roots [x,y,z]: ");
disp(x_curr);
disp ("Error (unit: %) [x,y,z]: ");
disp(error_vec);
disp ("# of iterations: ");
disp(count);

%% Gauss Elimination w/ partial pivoting
function [y] = gauss_elim (A,b) 

% b was supposed to be transposed since A = 3x3 and b = 1x3 - Maria Hanna
Ab = [A, b'];
%find the number of rows
no_rows = size (A,1);

    for  i = 1:(no_rows)
       %partial pivoting starts
       alpha = abs(Ab);
        %alpha is the column vector that will be used to compare with the pivot
        %element of the matrix
       alpha = alpha (i+1:end,i);
       if (max(alpha) > abs (Ab(i,i)))
            max_index = find(alpha == max(alpha));
            disp(max_index);
            %add 1 to the maximum index because the alpha vector has 1 element
            %less than the column where the pivot is situated at - this was
            %determined during set up of alpha in line 33

            %swap the 2 rows
            temp = Ab(max_index+1, :);
            Ab(max_index+1,:) = Ab(i,:);
            Ab(i, :) = temp;
        end
        %partial pivoting completed
        %row-reduction starts
          factor = Ab (i+1:end,i) / Ab(i,i);
         

          if i<no_rows
          Ab (i+1:end, :) = Ab (i+1:end,:) - factor .* Ab(i,:);
          end
          %row-reduction ends
          
    end

    y = zeros (no_rows,1);
    %find the last root using the last row
    y(end) = Ab (end, end) / Ab (end, end-1);
    %below is for backward substitution
    
    for j = (no_rows-1):-1:1
        %y (j) =round( ( Ab (j, end) -  Ab (j,j+1:no_rows) * y (j+1:no_rows))/ Ab (j, j),5,'significant');
    y (j) = (Ab (j, end) -  Ab (j,j+1:no_rows) * y (j+1:no_rows))/ Ab (j, j);
    end
    % Previously you defined x_curr and x_prev as 1x3 but this y is defined
    % as 3x1 so it was causing a lot of issues so I transposed it - Maria Hanna  
    %Got it - Thanks a lot! - Jia Hua J.
    y = y';
end